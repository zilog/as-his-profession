$(function() {
  $("#btnSubmit").click(function() {
		$.ajax({
		           type: "POST",
		           url: "index.php",
		           data: $("#frmContribution").serialize(), // serializes the form's elements.
		           success: function(data)
		           {
									//alert(data);
									var js = $.parseJSON(data);
									if("error" in js) {
										alert(js.error);
									} else {
										// no errors
										$("#test").html(js.html);
									}
		           }
		         });

		    return false; // avoid to execute the actual submit of the form.  
		});
});
