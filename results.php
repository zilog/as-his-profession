<?php require_once('config.php'); ?>
<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta
      name="viewport"
      content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"
    />
    <title>CATCHPA results</title>
		<script type="text/javascript" src="<?= $config['jquery_url'] ?>"></script>
		<link rel="stylesheet" href="style.css" type="text/css"></link>
		<style type="text/css">
		body {
			background: #f0f0f0 !important;
		}
		</style>
</head>
<body>
	<div id="global-container">
		
<?php
require_once('functions.php');

db_open();
echo db_get_last_three();

$options = array('caption' => "Responses so far");
db_showall($options);
db_close();

?>
			
	</div>
</body>
</html>

