<?php require_once('config.php'); ?>
<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta
      name="viewport"
      content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"
    />
    <title>CATCHPA</title>
		<script type="text/javascript" src="<?= $config['jquery_url'] ?>"></script>
		<link rel="stylesheet" href="style.css" type="text/css"></link>
		<script type="text/javascript" src="common.js"></script>
</head>
<body>
	<div id="global-container">
		
		<div id="test">
			<div id="imageArea">
				<img src="resources/img/base.png" alt="" class="center" />
			</div>
			<div class="formContainer">
				<form id="frmContribution" action="<?php echo $_SERVER["REQUEST_URI"] ?>" method="post">
					<p>Please guess what is the relationship between the two items shown in the picture above.</p>
					<fieldset>
						<div class="required">
							<label for="human">What's the human's profession?</label>
							<input type="text" name="human" id="human" class="inputText" size="40" maxlength="255" value="" placeholder="e.g. nurse">
						</div>
						<div class="required">
							<label for="doing">What's the computer doing?</label>
							<input type="text" name="doing" id="doing" class="inputText" size="40" maxlength="255" value="" placeholder="e.g. searching patient records">
						</div>
						<div class="required catchpa">
							<p>Now please prove that you are human...</p>
							<?php echo recaptcha_get_html($config['recaptcha']['public_key']); ?>
							</div>
							<div class="buttons">
								<input id="btnSubmit" type="submit" value="I solemnly submit &#187;" />
							</div>
					</fieldset>
				</form>
			</div>
		</div>
		
	</div>
</body>
</html>
