<?php

require_once('config.php');
require_once('lib/kint/Kint.class.php');

$phrases = array(
	  "Another visitor thought that the computer was <em>$2</em> and the human was a <em>$1</em>",
		"Someone said recently that the human's profession was <em>$1</em> and that the computer was <em>$2</em>",
		"Someone else said that the computer was <em>$2</em> and that the human was a <em>$1</em>",
		"Others thought that the human was a <em>$1</em> and the computer was <em>$2</em>",
		"Lately someone thought that the human was a <em>$1</em> and the computer was <em>$2</em>",
);

$db = NULL;

function phrase_compose($one, $two) {
	global $phrases;
	
	$idx = rand(0, (count($phrases)-1) );
	$phrase = $phrases[$idx];
	$tmp = str_replace("$1", stripslashes($one), $phrase);
	return str_replace("$2", stripslashes($two), $tmp);
}

function db_open() {
	global $db, $config;
	
	try
	{
		$connstr = "sqlite:{$config['db']['file_name']}";
		//dd($config);
		//echo "{$connstr}<br/>";
		$db = new PDO( $connstr );
	}
	catch(PDOException $e)
	{
		print 'Exception : '.$e->getMessage();
	}
}

function db_create_table() {
	global $db;

  $db->exec("CREATE TABLE IF NOT EXISTS guesses (id INTEGER PRIMARY KEY, human TEXT, machine TEXT, created_by_ip TEXT, created_on TIMESTAMP DEFAULT CURRENT_TIMESTAMP)"); 
}

function db_insert($human, $machine) {
	global $db, $_SERVER;

	$insert = "INSERT INTO guesses (human, machine, created_by_ip) 
					           VALUES (:human, :machine, :created_by_ip)";
	$stmt = $db->prepare($insert);

	// Bind values directly to statement variables
  $stmt->bindValue(':human', $human, SQLITE3_TEXT);
  $stmt->bindValue(':machine', $machine, SQLITE3_TEXT);
  $stmt->bindValue(':created_by_ip', $_SERVER['REMOTE_ADDR'], SQLITE3_TEXT);
  $stmt->execute();
}

function db_close() {
	global $db;

	$db = NULL;
}

function db_showall($opts) {
	global $db;

	// Select all data from memory db messages table 
  $result = $db->query('SELECT * FROM guesses');
	$r = $result->fetchAll();
	$idx = 0;
	if( isset($r) && (count($r) > 0) ) {
		echo '<table class="table">';
		if(isset($opts['caption'])) { echo "<caption>{$opts['caption']}</caption>"; }
		echo '<thead><tr>';
			echo '<th>id</th>';
			echo '<th>human</th>';
			echo '<th>machine</th>';
			echo '<th>ip</th>';
			echo '<th>created on</th>';
		echo '</tr></thead>';
		echo '<tbody>';
		  foreach($r as $row) {
  			echo ( ($idx % 2) ) ? '<tr class="even">' : '<tr class="odd">';
		    echo "<td>{$row['id']}</td>\n";
		    echo "<td>{$row['human']}</td>\n";
		    echo "<td>{$row['machine']}</td>\n";
		    echo "<td>{$row['created_by_ip']}</td>\n";
		    echo "<td>{$row['created_on']}</td>\n";
  			echo '</tr>';
				$idx++;
		  }
		echo '</tbody>';
		echo '</table>';
	} else {
		echo "<h3>Dataset is empty</h3>";
	}
}

function db_get_last_three() {
	global $db;
	
	$retval = NULL;
	
  $result = $db->query('SELECT `human`, `machine`, `created_on` FROM guesses ORDER BY `created_on` DESC LIMIT 3');
	$r = $result->fetchAll();
	if( isset($r) && (count($r) > 0) ) {
		$retval = '<dl class="last-guesses">';
		  foreach($r as $row) {
				$text = phrase_compose($row['human'], $row['machine']);
		    $retval .= "<dt>{$text}</dt>\n";
		  }
		$retval .= '</dl>';
	} else {
		$retval = "<h3>Dataset is empty</h3>";
	}
	
	return $retval;
}

function post_param_present($pname) {
	return ( isset($_POST[$pname]) && !empty($_POST[$pname]) );
}

function json_response($arr) {
	header('content-type: application/json; charset=utf-8');
	echo utf8_encode( json_encode($arr) );
}

?>