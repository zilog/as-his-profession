<?php
require('config.php');
require_once('lib/recaptcha/recaptchalib.php');
?>

<?php

//echo "{$_SERVER['REQUEST_METHOD']}"; die;

if( $_SERVER['REQUEST_METHOD'] === 'GET' ) {
	include('index.template.php');
} else if ( $_SERVER['REQUEST_METHOD'] === 'POST' ) {
	// save request to db
	// return json to replace content
	include('handler.post.php');
}
?>