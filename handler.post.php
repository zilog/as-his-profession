<?php

require_once('config.php');
require_once('lib/recaptcha/recaptchalib.php');
require_once('lib/kint/Kint.class.php');
require_once('functions.php');
require_once('oembed.php');

$resp = recaptcha_check_answer($config['recaptcha']['private_key'],
                              $_SERVER["REMOTE_ADDR"],
                              $_POST["recaptcha_challenge_field"],
                              $_POST["recaptcha_response_field"]);

if (!$resp->is_valid) {
  // bad CAPTCHA
	$resp = array('error' => "The reCAPTCHA wasn't entered correctly. Go back and try it again.");
	json_response($resp);
} else {
	db_open();

	try {
		db_create_table();
		if(post_param_present('human') && post_param_present('doing')) {
			db_insert($_POST['human'], $_POST['doing']);

			$resp = array();
			$html = '<div id="result-container">';
			$html .= '<div class="props"><h2>Thank you very much for taking part!</h2></div>';
			$html .= '<div id="results">';
			$html .= oembed_html('http://www.youtube.com/watch?v=EC5sbdvnvQM'); //"http://www.youtube.com";
			$html .= '<p>Here is what the last three participants said:</p>';
			$html .= db_get_last_three();
			$html .= '</div>';
			$html .= '<div id="share">';
			$html .= '</div></div>';
			
			$resp['html'] = $html;
			json_response($resp);
		} else {
			$errors = array();
			if(!post_param_present('human')) { $errors['human'] = "Surely this human must be doing something! Please tell us what his profession might be"; } 
			if(!post_param_present('doing')) { $errors['doing'] = "This machine is clearly busy, please tell us what you think it's doing"; } 
			$resp = array('error' => "One or more of the input fields is empty, please complete the form before submitting.");
			$resp['errors'] = $errors;
			json_response($resp);
		}
	} catch(PDOException $e) {
		print( $e->getMessage() );
	}

	db_close();
}

?>